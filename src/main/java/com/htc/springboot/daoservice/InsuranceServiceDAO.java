package com.htc.springboot.daoservice;

import java.util.List;

import com.htc.springboot.to.CustomerTO;
import com.htc.springboot.to.PolicyTO;

public interface InsuranceServiceDAO {
	public boolean addCustomer(CustomerTO customerTO);
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO);
	public List<PolicyTO> getInsurancePolicies(String customerCode);
	public PolicyTO getInsurancePolicy(long policyNo);
	public CustomerTO getCustomerDetails(String customerCode);
	public boolean takeNewPolicy(CustomerTO customerTO,PolicyTO policyTO);
}
