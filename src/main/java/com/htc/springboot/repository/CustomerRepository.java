package com.htc.springboot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.htc.springboot.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String>{

}
