package com.htc.springboot.service;


import java.util.Set;

import com.htc.springboot.model.Policy;
import com.htc.springboot.to.CustomerTO;
import com.htc.springboot.to.PolicyTO;

public interface InsuranceService {
	public boolean addCustomer(CustomerTO customerTO);
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO);
	public Set<Policy> getInsurancePolicies(String customerCode);
	public PolicyTO getInsurancePolicy(long policyNo);
	public CustomerTO getCustomerDetails(String customerCode);
	public boolean takeNewPolicy(CustomerTO customerTO,PolicyTO policyTO);
}
